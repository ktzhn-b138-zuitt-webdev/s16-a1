let fiftyfif = Number(prompt("Give me a number"));

console.log("The number you provided is " + fiftyfif);
for(fiftyfif ; fiftyfif >=50; fiftyfif--){
	console.log(fiftyfif);

	if (fiftyfif === 50){
		console.log("The current value is at 50. Terminating the loop.")
	}

	else if(fiftyfif % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.")
	}

	else if (fiftyfif % 5 === 0){
		console.log(fiftyfif);
	}


}

// For Loop END

/*4. Given a string, create a For Loop that will take take all the consonants of the string and store it in a different variable.*/

let initialWord = "supercalifragilisticexpialidocious";
let finalWord = '';

console.log (initialWord);

for(let i = 0; i < initialWord.length; i++){
	if (
		initialWord[i].toLowerCase() != "a" &&
		initialWord[i].toLowerCase() != "e" &&
		initialWord[i].toLowerCase() != "i" &&
		initialWord[i].toLowerCase() != "o" &&
		initialWord[i].toLowerCase() != "u" 	
	){
		finalWord = finalWord + initialWord[i]
	} 

}

console.log (finalWord);